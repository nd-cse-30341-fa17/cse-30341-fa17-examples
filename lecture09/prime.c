/* prime.c: brute-force prime finder */

#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>

#define max(a, b)   ((a) > (b) ? (a) : (b))

const size_t NCPUS     = 8;
const size_t PRIME_MAX = 400000;

size_t is_prime(size_t n) {
    for (size_t i = 2; i < n; i++) {
    	if (n % i == 0)
    	    return 0;
    }
    return 1;
}

/* Single-threaded
int main(int argc, char *argv[]) {
    size_t count = 0;
    for (size_t i = 2; i < PRIME_MAX; i++) {
	count += is_prime(i);
    }
    printf("There are %lu primes less than %lu\n", count, PRIME_MAX);
    return 0;
}
*/

/* Multi-threaded (locks) */

struct prime_args {
    size_t start;
    size_t stop;
};

size_t Count = 0;
pthread_mutex_t Lock = PTHREAD_MUTEX_INITIALIZER;

void * count_primes(void *arg) {
    struct prime_args *pa = (struct prime_args *)arg;
    size_t count = 0;
    printf("start=%lu, stop=%lu\n", pa->start, pa->stop);
    for (size_t n = pa->start; n < pa->stop; n++) {
    	/*
    	pthread_mutex_lock(&Lock);
    	Count += is_prime(n);
    	pthread_mutex_unlock(&Lock);
    	*/
    	count += is_prime(n);
    }
    	
    pthread_mutex_lock(&Lock);
    Count += count;
    pthread_mutex_unlock(&Lock);

    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t threads[NCPUS];
    struct prime_args args[NCPUS];
    size_t count = 0;

    for (size_t i = 0; i < NCPUS; i++) {
    	args[i].start = max(2, i*PRIME_MAX/NCPUS);
    	args[i].stop  = (i+1)*PRIME_MAX/NCPUS;
    	pthread_create(&threads[i], NULL, count_primes, &args[i]);
    }

    for (size_t i = 0; i < NCPUS; i++) {
    	pthread_join(threads[i], NULL);
    }

    printf("There are %lu primes less than %ld\n", Count, PRIME_MAX);
    return 0;
} 

/* Multi-threaded (no locks)

void * count_primes(void *arg) {
    struct prime_args *pa = (struct prime_args *)arg;
    size_t count = 0;
    printf("start=%lu, stop=%lu\n", pa->start, pa->stop);
    for (size_t n = pa->start; n < pa->stop; n++) {
    	count += is_prime(n);
    }

    return (void *)count;
}

int main(int argc, char *argv[]) {
    pthread_t threads[NCPUS];
    struct prime_args args[NCPUS];
    size_t count = 0, result;

    for (size_t i = 0; i < NCPUS; i++) {
    	args[i].start = max(2, i*PRIME_MAX/NCPUS);
    	args[i].stop  = (i+1)*PRIME_MAX/NCPUS;
    	pthread_create(&threads[i], NULL, count_primes, &args[i]);
    }

    for (size_t i = 0; i < NCPUS; i++) {
    	pthread_join(threads[i], (void **)&result);
    	count += result;
    }

    printf("There are %lu primes less than %ld\n", count, PRIME_MAX);
    return 0;
} 
*/
