// queue.h: Queue

#pragma once

#include "macros.h"

#include <queue>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <semaphore.h>

const size_t QUEUE_MAX = 64;

template <typename T>
class Queue {
private:
    std::queue<T>   data;
    T		    sentinel;
    sem_t	    mutex;
    sem_t	    full;
    sem_t	    empty;

public:
    Queue(T s) : sentinel(s) {
    	sem_init(&mutex, 0, 1);
    	sem_init(&full , 0, 0);
    	sem_init(&empty, 0, QUEUE_MAX);
    }

    T pop() {
    	sem_wait(&full);

    	sem_wait(&mutex);
	T value = data.front();
	if (value != sentinel) {
	    data.pop();
	}
    	sem_post(&mutex);

	if (value != sentinel) {
	    sem_post(&empty);
	} else {
	    sem_post(&full);
	}
    	return value;
    }

    void push(const T &value) {
    	sem_wait(&empty);
    	sem_wait(&mutex);

	data.push(value);

    	sem_post(&mutex);
    	sem_post(&full);
    }
};
