// psgrep.cpp

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

// Main Execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    if (argc < 2) {
    	fprintf(stderr, "Usage: psgrep phrase\n");
    	return EXIT_FAILURE;
    }

    // Create Pipe
    int pipefd[2];
    if (pipe(pipefd)) {
    	perror("pipefd");
    	return EXIT_FAILURE;
    }

    // Fork and execute
    pid_t pid = fork();
    if (pid == 0) {         // Child
        close(pipefd[1]);               // Close pipe write end
        dup2(pipefd[0], STDIN_FILENO);  // Replace STDIN with pipe read end
        execlp("grep", "grep", argv[1], NULL);
        perror("execlp");
        _exit(EXIT_FAILURE);
    } else if (pid > 0) {   // Parent
        close(pipefd[0]);               // Close pipe read end
        dup2(pipefd[1], STDOUT_FILENO); // Replace STDOUT with pipe write end
        execlp("ps", "ps", "ux", NULL);
        perror("execlp");
        _exit(EXIT_FAILURE);
    } else {                // Error
        perror("fork");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

// vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: ----------------------------------
