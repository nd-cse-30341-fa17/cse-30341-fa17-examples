/* idlebin.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <libgen.h>
#include <unistd.h>

/* true *---------------------------------------------------------------------*/

int do_true(int argc, char *argv[]) {
    return EXIT_SUCCESS;
}
/* cat *----------------------------------------------------------------------*/

int do_cat_fd(int fd) {
    char buffer[BUFSIZ];
    int nread;

    while ((nread = read(fd, buffer, BUFSIZ)) > 0) {
    	write(STDOUT_FILENO, buffer, nread);
    }

    return EXIT_SUCCESS;
}

int do_cat(int argc, char *argv[]) {
    if (argc == 1) {
    	return do_cat_fd(STDIN_FILENO);
    }

    int result = 0;
    for (int i = 1; i < argc; i++) {
    	int fd = open(argv[i], O_RDONLY);
    	if (fd < 0) {
    	    fprintf(stderr, "Unable to open %s: %s\n", argv[i], strerror(errno));
    	    return EXIT_FAILURE;
	}

	result += do_cat_fd(fd);
	close(fd);
    }

    return result;
}

/* test *---------------------------------------------------------------------*/

int do_test(int argc, char *argv[]) {
    if (argc != 3 || strlen(argv[1]) < 2) {
    	fprintf(stderr, "Usage: %s [-e -x -r -d -f -s] path\n", argv[0]);
    	return EXIT_FAILURE;
    }

    const char *flag = argv[1];
    const char *path = argv[2];
    struct stat s;

    switch (flag[1]) {
	case 'e':
	    return stat(path, &s);
	case 'r':
	    return access(path, R_OK);
	case 'x':
	    return access(path, X_OK);
	case 'd':
	    return stat(path, &s) || !S_ISDIR(s.st_mode);
	case 'f':
	    return stat(path, &s) || !S_ISREG(s.st_mode);
	case 's':
	    return stat(path, &s) || s.st_size == 0;
    }

    return EXIT_FAILURE;
}

/* ls *-----------------------------------------------------------------------*/

int do_ls(int argc, char *argv[]) {
    char *path = argc > 1 ? argv[1] : ".";
    DIR  *d    = opendir(path);

    if (d == NULL) {
    	fprintf(stderr, "Unable to opendir %s: %s\n", path, strerror(errno));
	return EXIT_FAILURE;
    }

    struct dirent *e;
    while ((e = readdir(d))) {
    	
    	if (strcmp(".", e->d_name) == 0 || strcmp("..", e->d_name) == 0)
    	    continue;

	puts(e->d_name);
    }
    closedir(d);
    return EXIT_SUCCESS;
}

/* Main Execution *-----------------------------------------------------------*/

int main(int argc, char *argv[]) {
    char *command = basename(argv[0]);

    if (strcmp(command, "true") == 0) {
    	return do_true(argc, argv);
    } else if (strcmp(command, "cat") == 0) {
    	return do_cat(argc, argv);
    } else if (strcmp(command, "test") == 0) {
    	return do_test(argc, argv);
    } else if (strcmp(command, "ls") == 0) {
    	return do_ls(argc, argv);
    } else {
    	fprintf(stderr, "Unknown command: %s\n", command);
    	return EXIT_FAILURE;
    }
}
