/* malloc.c: simple memory allocator -----------------------------------------*/

#include <assert.h>
#include <unistd.h>

/* Allocate space ------------------------------------------------------------*/

void *malloc(size_t size) {
    void *curr = sbrk(0);
    void *prev = sbrk(size);

    if (prev == (void *)-1) {    /* OS allocation failed */
    	return NULL;
    }

    return curr;
}

/* Reclaim space -------------------------------------------------------------*/

void free(void *ptr) {
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: --------------------------------*/
