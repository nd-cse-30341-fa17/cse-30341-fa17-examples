// queue2.h: Mostly Working Queue (need to becareful of waking up right threads)

#pragma once

#include "macros.h"

#include <queue>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

const size_t QUEUE_MAX = 64;

template <typename T>
class Queue {
private:
    std::queue<T>   data;
    T		    sentinel;
    pthread_mutex_t lock;
    pthread_cond_t  cond;

public:
    Queue(T s) : sentinel(s) {
    	int rc;
    	Pthread_mutex_init(&lock, NULL);
    	Pthread_cond_init(&cond, NULL);
    }

    void push(const T &value) {
    	int rc;
    	
    	Pthread_mutex_lock(&lock);
    	while (data.size() >= QUEUE_MAX) {
    	    Pthread_cond_wait(&cond, &lock);
	}

	data.push(value);
    	Pthread_cond_signal(&cond);
    	Pthread_mutex_unlock(&lock);
    }
    
    T pop() {
    	int rc;

    	Pthread_mutex_lock(&lock);
    	while (data.empty()) {
    	    Pthread_cond_wait(&cond, &lock);
	}

	T value = data.front();
	if (value != sentinel) {
	    data.pop();
	}
    	Pthread_cond_signal(&cond);
    	Pthread_mutex_unlock(&lock);
    	return value;
    }
};
