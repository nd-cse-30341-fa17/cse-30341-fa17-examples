// queue0.h: Racy Queue (no locks)

#pragma once

#include <queue>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

const size_t QUEUE_MAX = 64;

template <typename T>
class Queue {
private:
    std::queue<T> data;
    T		  sentinel;

public:
    Queue(T s) : sentinel(s) {}

    void push(const T &value) {
    	while (data.size() >= QUEUE_MAX) {
    	    // Spin
	}

	data.push(value);
    }
    
    T pop() {
    	while (data.empty()) {
    	    // Spin
	}

	T value = data.front();
	if (value != sentinel) {
	    data.pop();
	}
    	return value;
    }
};
