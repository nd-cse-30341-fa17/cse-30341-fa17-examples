// timeout.cpp

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

// Global variables -------------------------------------------------------------

int         Timeout = 10;
const char *Input   = NULL;
const char *Output  = NULL;
const char *Command = NULL;

// Parse command line options ---------------------------------------------------

bool parse_command_line_options(int argc, char *argv[]) {
    int argind = 1;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
	char *arg = argv[argind++];

	switch (arg[1]) {
	    case 't':
	    	Timeout = atoi(argv[argind++]);
	    	break;
	    case 'i':
	    	Input = argv[argind++];
	    	break;
	    case 'o':
	    	Output = argv[argind++];
	    	break;
	    default:
	    	fprintf(stderr, "Usage: timeout -t SECONDS -i INPUT -o OUTPUT command\n");
	    	return false;
	}
    }

    if (argind < argc) {
	Command = argv[argind++];
    }

    if (Input == NULL && Output == NULL && Command == NULL) {
    	return false;
    }

    return true;
}

// Alarm Handler ---------------------------------------------------------------

void alarm_handler(int signum) {
    fprintf(stderr, "Timeout after %d seconds!\n", Timeout);
    exit(EXIT_FAILURE);
}

// Main Execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    if (!parse_command_line_options(argc, argv)) {
    	return EXIT_FAILURE;
    }

    // Arm alarm
    signal(SIGALRM, alarm_handler);
    alarm(Timeout);

    /* Version 1: system
    char command[BUFSIZ];
    snprintf(command, BUFSIZ, "%s < %s > %s", Command, Input, Output);
    system(command);
    */

    /* Version 2: fork/exec
    char command[BUFSIZ];
    snprintf(command, BUFSIZ, "%s < %s > %s", Command, Input, Output);

    pid_t pid = fork();
    switch (pid) {
        case -1:        // Error
            fprintf(stderr, "Unable to fork: %s\n", strerror(errno));
            break;
        case  0:        // Child
            execlp("/bin/sh", "/bin/sh", "-c", command, NULL);
            _exit(EXIT_FAILURE);
            break;
        default:        // Parent
            wait(NULL);
            break;
    }
    */

    /* Version 3: fork/exec/dup2 */
    int input_fd  = open(Input , O_RDONLY);                 // FIXME: error check
    int output_fd = open(Output, O_WRONLY|O_CREAT, 0644);   // FIXME: error check

    pid_t pid = fork();
    switch (pid) {
        case -1:        // Error
            fprintf(stderr, "Unable to fork: %s\n", strerror(errno));
            break;
        case  0:        // Child
            dup2(input_fd , STDIN_FILENO);                          // FIXME: error check
            dup2(output_fd, STDOUT_FILENO);                         // FIXME: error check
            execlp("/bin/sh", "/bin/sh", "-c", Command, NULL);
            _exit(EXIT_FAILURE);
            break;
        default:        // Parent
            close(input_fd);
            close(output_fd);
            wait(NULL);
            break;
    }

    return EXIT_SUCCESS;
}

// vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: ----------------------------------
