#!/bin/sh

do_test() {
    flag=$1
    path=$2

    echo -n "Testing $flag ... "
    if ./check $flag $path && test $flag $path && ! ./check $flag asdf; then
	echo "Success"
    else
	echo "Failure"
    fi
}

do_test -e hello.c
do_test -r hello.c
do_test -x hello
do_test -d /etc
do_test -f /etc/hosts
do_test -s /etc/hosts
