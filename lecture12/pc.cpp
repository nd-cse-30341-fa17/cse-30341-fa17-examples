// pc.cpp: Producer / Consumer

#include "queue.h"
#include "thread.h"

const size_t NITEMS   = 1000;
const size_t NTHREADS = 4;

// Producer Thread

void *producer(void *arg) {
    Queue<int> *q = (Queue<int> *)arg;

    for (size_t i = 0; i < NITEMS; i++) {
    	q->push(i);
    }

    q->push(-1);
    return 0;
}

// Consumer Thread

void *consumer(void *arg) {
    Queue<int> *q = (Queue<int> *)arg;
    size_t  total = 0;
    int     value = 0;

    while (true) {
	value = q->pop();
	if (value >= 0)
	    total += value;
	else 
	    break;
    }
    
    return (void *)total;
}

// Main execution

int main(int argc, char *argv[]) {
    Queue<int>	q(-1);
    Thread	t[NTHREADS];

    t[0].start(producer, &q);
    for (size_t i = 1; i < NTHREADS; i++) {
	t[i].start(consumer, &q);
    }

    size_t result, total = 0;
    for (size_t i = 0; i < NTHREADS; i++) {
	t[i].join((void **)&result);
	total += result;
    }

    printf("total = %lu\n", total);
    return 0;
}
