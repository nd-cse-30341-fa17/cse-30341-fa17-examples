// queue1.h: Deadlocked Queue (Spinning with locks)

#pragma once

#include "macros.h"

#include <queue>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

const size_t QUEUE_MAX = 64;

template <typename T>
class Queue {
private:
    std::queue<T>   data;
    T		    sentinel;
    pthread_mutex_t lock;

public:
    Queue(T s) : sentinel(s) {
    	int rc;
    	Pthread_mutex_init(&lock, NULL);
    }

    void push(const T &value) {
    	int rc;
    	
    	Pthread_mutex_lock(&lock);
    	while (data.size() >= QUEUE_MAX) {
    	    // Spin
	}

	data.push(value);
    	Pthread_mutex_unlock(&lock);
    }
    
    T pop() {
    	int rc;

    	Pthread_mutex_lock(&lock);
    	while (data.empty()) {
    	    // Spin
	}

	T value = data.front();
	if (value != sentinel) {
	    data.pop();
	}
    	Pthread_mutex_unlock(&lock);
    	return value;
    }
};
